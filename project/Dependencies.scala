import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1"
  lazy val httpClient = "org.scalaj" %% "scalaj-http" % "2.3.0"
}
