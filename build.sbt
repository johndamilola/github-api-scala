name := "scala-calc"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test",
  "org.scalaj" %% "scalaj-http" % "2.3.0",
  "com.typesafe.play" %% "play-json" % "2.3.0"
)

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
  
