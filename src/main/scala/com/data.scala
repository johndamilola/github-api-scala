package tools.results

import scalaj.http._
import play.api.libs.json._

object Github {

    def getData(username: String): play.api.libs.json.JsValue = {
        
        // val response: HttpResponse[String] = Http("https://api.github.com/search/repositories").param("q","scala").asString
        val url = s"https://api.github.com/users/$username/following"
        val response: HttpResponse[String] = Http(url).asString

        val token = response.body
        val rawJson = s"""$token"""

        return Json.parse(rawJson)
    }
}

// GET Request
// val response: HttpResponse[String] = Http("http://foo.com/search").param("q","monkeys").asString

// POST Request
// val postRequest = Http("http://foo.com/add").postForm(Seq("name" -> "jon", "age" -> "29")).asString