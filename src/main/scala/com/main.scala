import tools.results.Github
import tools.results.Stats

object Idea {
    def main(args: Array[String]) = {
        // get username
        val username: String = readLine("Enter your username")
        // println(Github.getData(username)(0))
        Stats(Github.getData(username)(0))
    }
}

// GET Request
// val response: HttpResponse[String] = Http("http://foo.com/search").param("q","monkeys").asString

// POST Request
// val postRequest = Http("http://foo.com/add").postForm(Seq("name" -> "jon", "age" -> "29")).asString